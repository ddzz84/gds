<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('event', 'EventController@index'); // get today evets
Route::get('event/{day}', 'EventController@show'); // get day event (1=today, 2=tomorrow, 3=>next day, ecc)
// not implemented
// Route::post('event', 'EventController@store');
// Route::put('event/{day}', 'EventController@update');
// Route::delete('event/{day}', 'EventController@delete');
