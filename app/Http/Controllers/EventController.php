<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class EventController extends Controller
{
    //
    public function index()
    {
        return Event::all();
    }

    public function show($day){
        // start from 1 = today
        return Event::find(['event_day'=>strtotime('+'.($day-1).'day')]);
    }

    public function store(Request $request)
    {
        // TODO: implement
    }

    public function update(Request $request, Event $event)
    {
      // TODO: implement
    }

    public function delete(Event $event)
    {
      // TODO: implement
    }
}
