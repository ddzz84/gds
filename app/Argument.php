<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Argument extends Model
{
    //
    protected $fillable = ['title', 'list_author'];

}
