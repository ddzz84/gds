<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model{
    //
    protected $fillable = ['name', 'event_date', 'time_in', 'time_out'];

    public function rooms(){
       return $this->belongsTo('App\Room');
   }

    public function chairs(){
        return $this->belongsToMany('App\Chair'); //->withPivot('extraField');
    }

    public function arguments(){
        return $this->belongsToMany('App\Argument'); //->withPivot('extraField');
    }
}
