<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    //

    protected $fillable = ['name', 'available'];

    public function events() {
        return $this->hasMany('App\Event');
    }
}
